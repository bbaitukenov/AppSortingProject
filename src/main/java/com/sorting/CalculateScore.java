package com.sorting;

public class CalculateScore {

    private int score;


    public void calculateScore(int mathScore, int englishScore) {
        if(mathScore < 0 || mathScore > 100 || englishScore < 0 || englishScore > 100) {
            score = -1;
        } else {
            score = mathScore * englishScore;
        }

    }

    public int getScore() {
        return this.score;
    }


}