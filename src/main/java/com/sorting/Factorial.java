package com.sorting;

public class Factorial {

    private int result;

    public int factorial(int number) {
        int num = 1;
        for (int i = 1; i <= number; i++) {
            num *= i;
        }
        return num;
    }

}
