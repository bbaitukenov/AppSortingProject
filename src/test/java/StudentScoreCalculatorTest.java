import com.sorting.CalculateScore;
import org.junit.Test;

import static org.junit.Assert.*;

public class StudentScoreCalculatorTest {

    @Test
    public void studentScoreCalculatorRegular() {
        CalculateScore cs = new CalculateScore();
        cs.calculateScore(50, 50);
        assertEquals(2500, cs.getScore());
    }

    @Test
    public void studentScoreCalculatorMathHigh() {
        CalculateScore cs = new CalculateScore();
        cs.calculateScore(102, 50);
        assertEquals(-1, cs.getScore());
    }

    @Test
    public void studentScoreCalculatorEnglishHigh() {
        CalculateScore cs = new CalculateScore();
        cs.calculateScore(50, 150);
        assertEquals(-1, cs.getScore());
    }

    @Test
    public void studentScoreCalculatorBothHigh() {
        CalculateScore cs = new CalculateScore();
        cs.calculateScore(150, 150);
        assertEquals(-1, cs.getScore());
    }

}
