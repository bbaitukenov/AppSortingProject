import com.sorting.Factorial;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CornerCaseTest {

    Factorial fc = new Factorial();

    @Test
    public void testFactorial() {


        assertEquals(1, fc.factorial(0));

        assertEquals(1, fc.factorial(1));

        assertEquals(2, fc.factorial(2));

        assertEquals(6, fc.factorial(3));

        assertEquals(24, fc.factorial(4));

        assertEquals(3628800, fc.factorial(10));
    }

}
