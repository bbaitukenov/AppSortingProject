import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.junit.Test;


public class Log4jTest {
    private static final Logger logger = Logger.getLogger(String.valueOf(Log4jTest.class));

    @Test
    public void testLog4j() {
        BasicConfigurator.configure();
        logger.debug("This is a debug message");
        logger.info("This is an info message");
        logger.warn("This is a warning message");
        logger.error("This is an error message");
        logger.fatal("This is a fatal message");
    }
}